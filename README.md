# README #

### What is this repository for? ###

* For anyone with a Saitek X-55 that's playing Elite Dangerous

### How do I get set up? ###

* Download the file and put it into the Elite Dangerous' "Products/FORC-FDEV-D-1010/ControlSchemes" folder
* It's recommended that you visit http://www.mcdee.net/elite/ and get a reference sheet. This will make it a lot easier to learn the setup, in particular if you have multiple monitors, you can have the ref. sheet on the side.
* Once inside the game, go to control setup and in the drop-down box, choose "Saitek X-55 Custom".
* Play

### Who do I talk to? ###

* If you find an issue or have suggestions, add an issue to the tracker: https://bitbucket.org/Gordam/elite-dangerous-custom-setup/issues?status=new&status=open